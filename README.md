# GridTestProject iOS app

## Development
### Required tools
- Xcode 13.0 or higher

### Dependencies
There are no dependencies on 3rd party frameworks  

### Launching project
Project is optimalized for iPad devices. It is recomanded to launch application on iPad Pro (12.9 inch).

It possible to launch application on simulator via XCode. Select device or simulator you wish to launch application on and start build by selecting Product -> Run from menu or by using Cmd+R shortcut.

### Troubleshooting
Unfortunatelly iOS is not optimalized to display large number of views. There are currently more then 2.5K subivews on the grid so if applicaion seems to be lagging I reccomend to lower `gridDimension` value from 50 to smaller number.


## Project
### Architecture
- This project uses MVVM architecture. 
- There are used native SwiftUI+Combine frameworks (reactice principles)


## Functionality
- By default grid is filled with empty items.
- Tapping on grid item increment all values in row a column by one.
- After updating values app check if there is Fibonacci sequence somewhere in updated values. To lower computing time, only some part of grid are checked -> only these part that are in the renge of updated row and column.
- If Fibonacci sequence is found, items in Grid are set to default (empty) state.
- Reset button clears whole Grid (resets it to default state).  


## Known Issues
- There is an issue with animating background colors. Unfortunaltelly I was unabled to resolve it in the end. In some cases (where there was already some value in grid item) backgournd color returns to default color way to quickly.
