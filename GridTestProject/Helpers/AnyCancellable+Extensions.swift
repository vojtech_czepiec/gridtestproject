//
//  AnyCancellable+Extensions.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 06.12.2021.
//

import Combine

extension AnyCancellable {

    // Store subscriber into CancelBag
    func store(in cancelBag: CancelBag) {
        cancelBag.subscriptions.insert(self)
    }
}
