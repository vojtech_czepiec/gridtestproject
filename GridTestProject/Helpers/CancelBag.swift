//
//  CancelBag.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 06.12.2021.
//

import Combine

final class CancelBag {
    var subscriptions = Set<AnyCancellable>()

    // Cancel all stored subscriptions
    func cancel() {
        subscriptions.forEach { $0.cancel() }
        subscriptions.removeAll()
    }
}
