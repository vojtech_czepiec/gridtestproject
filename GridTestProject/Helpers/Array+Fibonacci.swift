//
//  Array+Fibonacci.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 06.12.2021.
//

import Foundation

// Extension for Array where elements are Int objects
extension Array where Element == Int {

    /// Returns `true` if Array (`self`) contains Fibonacci sequence. Otherwise returs `false`
    /// Returns `false` if Array contains less than three items.
    func isFibonacciSequence() -> Bool {
        let length = self.count

        guard
            length > 2, // Check sufficient Array length
            self.contains(where: { $0 != .zero }) // Check if Array does not contains only zeros
        else {
            return false
        }

        for index in 2...length-1 {
            // Returns false if number is not a sum of two previous numbers in sequence
            if self[index - 2] + self[index - 1] != self[index] {
                return false
            }
        }

        return true
    }
}
