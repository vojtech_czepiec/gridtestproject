//
//  GridTestProjectApp.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 06.12.2021.
//

import SwiftUI

@main
struct GridTestProjectApp: App {
    var body: some Scene {
        WindowGroup {
            GridView()
        }
    }
}
