//
//  ContentView.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 06.12.2021.
//

import SwiftUI
import Combine

struct GridView: View {

    // Observed ViewModel property
    @ObservedObject var viewModel: ViewModel = ViewModel()

    // Private properties
    private let itemSpacing: CGFloat = 5.0

    // Main grid layout body
    var body: some View {
        VStack(alignment: .center, spacing: itemSpacing) {
            resetButton
            ScrollView([.horizontal, .vertical], showsIndicators: true) {
                VStack(spacing: itemSpacing) {
                    ForEach(viewModel.gridItems, id: \.self) { row in
                        HStack(spacing: itemSpacing) {
                            ForEach(row, id: \.self) { itemViewModel in
                                GridItemView(gridViewModel: viewModel, itemViewModel: itemViewModel)
                            }
                        }
                    }
                }
                .padding(itemSpacing)
            }
            .background(Color.gray)
        }
    }

    private var resetButton: some View {
        Button(action: {
            viewModel.onResetButtonPressed()
        }) {
            Text("Reset")
        }
    }
}

// Helper struct to enable Preview functionality for GridView
struct GridView_Previews: PreviewProvider {
    static var previews: some View {
        GridView()
    }
}
