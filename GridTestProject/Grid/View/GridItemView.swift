//
//  GridItemView.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 07.12.2021.
//

import SwiftUI
import Combine

struct GridItemView: View {

    // Observed ViewModel properties
    @ObservedObject var gridViewModel: GridView.ViewModel
    @ObservedObject var itemViewModel: GridItemViewModel

    // Private properties
    private let itemDimension: CGFloat = 40.0
    private let fontSize: CGFloat = 20.0
    private let animationDuration: CGFloat = 0.5

    // Main grid layout body
    var body: some View {
        Button(action: {
            gridViewModel.onItemPressed(at: itemViewModel.point)
        }) {
            Text(itemViewModel.formattedValue)
                .font(.system(size: fontSize))
                .frame(width: itemDimension, height: itemDimension)
                .foregroundColor(.black)
                .background(itemViewModel.animate ? itemViewModel.color : Color.white)
                .animation(.easeIn(duration: animationDuration))
        }
        .disabled(itemViewModel.animate)
    }
}
