//
//  GridViewModel.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 06.12.2021.
//

import Combine
import SwiftUI

// ViewModel related to GridView
extension GridView {

    final class ViewModel: ObservableObject {

        // 2D array of `GridItemViewModel` data
        @Published var gridItems: [[GridItemViewModel]] = []

        // Publisher for last selected point
        @Published var selectedPoint: Point?

        // Specifies grid dimension
        let gridDimension: Int = 50

        // Private properties
        private let fibonacciSequenceLength: Int = 5
        private let cancelBag = CancelBag()

        // Initialization
        init() {
            gridItems = getDefaultGridItems()
            setupBinginds()
        }
    }
}

// Private ViewModel functions
extension GridView.ViewModel {

    // Returns 2D Array of GridItemViewModel with default data
    private func getDefaultGridItems() -> [[GridItemViewModel]] {
        var data: [[GridItemViewModel]] = []

        // Fills 2D array of GridItemViewModel objects with default values
        for x in 0..<gridDimension {
            var row: [GridItemViewModel] = []
            for y in 0..<gridDimension {
                row.append(
                    GridItemViewModel(
                        point: Point(vertical: x, horizontal: y),
                        color: .white,
                        animate: false
                    )
                )
            }
            data.append(row)
        }

        return data
    }

    private func setupBinginds() {
        // Subscribe to changes in `selectedPoint` to update stored data values
        $selectedPoint
            .compactMap { $0 } // Filter `nil` values
            .sink { point in self.updatedGridItems(for: point) }
            .store(in: cancelBag)

        // Subscribe to changes in `selectedPoint` to check if some parts of grid contains Fibonacci sequences
        $selectedPoint
            .combineLatest($gridItems)
            .sink { combined in
                guard let point = combined.0 else { return }
                self.checkFibonacciSequence(at: point, currentValues: combined.1)
            }
            .store(in: cancelBag)
    }

    // Updates values for items in grid accorind to selected point
    private func updatedGridItems(for point: Point) {
        // Increment values for items in both axis
        for(x, row) in gridItems.enumerated() {
            for(y, item) in row.enumerated() where x == point.vertical || y == point.horizontal {
                item.value = (item.value ?? 0) + 1
                item.color = .yellow
                item.animate = true
            }
        }

        // Assign updated items to propagate changes into UI
        Just(gridItems)
            .assign(to: &$gridItems)
    }

    // Checks Fibobbanci sequence in parts on grid affected by updated values after tap
    private func checkFibonacciSequence(at point: Point, currentValues: [[GridItemViewModel]]) {
        let columnChunks = getColumnChunks(from: point, currentValues: currentValues)
        let rowChunks = getRowChunks(from: point, currentValues: currentValues)

        rowChunks.forEach { row in
            var tempRow = row
            while tempRow.count > fibonacciSequenceLength-1 {
                let chunk = Array(row[0...fibonacciSequenceLength-1])
                checkFibonaciSequence(for: chunk)
                tempRow = Array(tempRow.dropFirst())
            }
        }

        columnChunks.forEach { column in
            var tempColumn = column
            while tempColumn.count > fibonacciSequenceLength-1 {
                let chunk = Array(column[0...fibonacciSequenceLength-1])
                checkFibonaciSequence(for: chunk)
                tempColumn = Array(tempColumn.dropFirst())
            }
        }

        // Assign updated items to propagate changes into UI
        Just(gridItems)
            .assign(to: &$gridItems)
    }

    // Returns some parts of grid (rows) which can contains Fibonacci sequences after update
    private func getRowChunks(
        from point: Point,
        currentValues: [[GridItemViewModel]]
    ) -> [[GridItemViewModel]] {
        var rowChunks: [[GridItemViewModel]] = []

        // Vertical offset determining the possible range where Fibonacci sequence can be found
        let leftOffset = point.horizontal - fibonacciSequenceLength + 1
        let rightOffset = point.horizontal + fibonacciSequenceLength - 1

        // Separates whole rows in chunks according to vertical offsets
        for row in currentValues {
            let chunk = row[max(0, leftOffset)...min(row.count - 1, rightOffset)]
            rowChunks.append(Array(chunk))
        }

        return rowChunks
    }

    // Returns some parts of grid (columns) which can contains Fibonacci sequences after update
    private func getColumnChunks(
        from point: Point,
        currentValues: [[GridItemViewModel]]
    ) -> [[GridItemViewModel]] {
        // Horizontal offset determining the possible range where Fibonacci sequence can be found
        let topOffset = point.vertical - fibonacciSequenceLength + 1
        let bottomOffset = point.vertical + fibonacciSequenceLength - 1

        // Separates whole rows in chunks according to offsets
        let chunk = currentValues[max(0, topOffset)...min(currentValues.count - 1, bottomOffset)]

        let rowChunks = Array(chunk)
        var columnChunks: [[GridItemViewModel]] = []

        // Remakes row arrays into column arrays to make check for Fibonacci sequence possible
        rowChunks.enumerated().forEach { x, row in
            row.enumerated().forEach { y, model in
                if x == 0 {
                    columnChunks.append([model])
                } else {
                    columnChunks[y].append(model)
                }
            }
        }

        return columnChunks
    }

    // Checks if `values` in array of `GridItemViewModel` contains Fibonacci sequence
    private func checkFibonaciSequence(for items: [GridItemViewModel]) {
        let values = items
            .map { $0.value } // Map to array of Int objects
            .compactMap { $0 } // Filter `nil` objects

        guard values.count == fibonacciSequenceLength else { return }

        if values.isFibonacciSequence() {
            items.forEach { model in
                model.value = nil
                model.color = .green
                model.animate = true
            }
        }
    }
}

// Public ViewModel functions
extension GridView.ViewModel {

    // Function called when particular grid cell is pressed
    func onItemPressed(at point: Point) {
        // Asign new value to `selectedPoint` Published property
        Just(point)
            .assign(to: &$selectedPoint)
    }

    // Resets grid to default (empty) state
    func onResetButtonPressed() {
        gridItems = getDefaultGridItems()
    }
}
