//
//  GridItemViewModel.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 06.12.2021.
//

import SwiftUI
import Combine

// View model for particular item in grid
final class GridItemViewModel: ObservableObject, Identifiable {

    // Public properties
    var point: Point
    var value: Int?
    var color: Color

    // Computed property with current value formated as String
    var formattedValue: String {
        guard let value = value else { return " " }

        // Ternary operator which returns empty whitespace if value is lower or equal to zero, otherwise
        // returns `value` as String
        return value <= 0 ? " " : "\(value)"
    }

    // Published property for triggering animation
    @Published var animate: Bool

    // Private properties
    private let animationTimePublisher = Timer.TimerPublisher(interval: 0.5, runLoop: .main, mode: .default)
    private var timerCancellable: AnyCancellable?
    private let cancelBag = CancelBag()

    // Inicialization
    init(
        point: Point,
        color: Color,
        animate: Bool
    ) {
        self.point = point
        self.color = color
        self.animate = animate

        setupBindings()
    }
}

// Private functions
extension GridItemViewModel {

    private func setupBindings() {
        $animate
            .removeDuplicates()
            .sink(receiveValue: { animate in
                if animate == true {
                    self.startTimer()
                } else {
                    self.timerCancellable?.cancel()
                }
            })
            .store(in: cancelBag)

        animationTimePublisher
            .map { _ in return false }
            .assign(to: &$animate)
    }

    private func startTimer() {
        timerCancellable = animationTimePublisher.connect() as? AnyCancellable
    }
}

// GridItemViewModel conforms Hashable protocol - enables easier UI contruction via `ForEach` in `GridView`
extension GridItemViewModel: Hashable {

    func hash(into hasher: inout Hasher) {
        hasher.combine(point)
    }

    static func == (lhs: GridItemViewModel, rhs: GridItemViewModel) -> Bool {
        lhs.point == rhs.point &&
        lhs.value == rhs.value &&
        lhs.color == rhs.color &&
        lhs.animate == rhs.animate
    }
}
