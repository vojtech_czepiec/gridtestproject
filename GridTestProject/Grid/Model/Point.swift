//
//  Point.swift
//  GridTestProject
//
//  Created by Vojtěch Czepiec on 06.12.2021.
//

// Struct for easier way to work with axis coordinates
struct Point: Hashable {
    let vertical: Int
    let horizontal: Int
}
